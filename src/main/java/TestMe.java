import java.util.Date;

/**
 * Created by e070769 on 4/26/17.
 */
public class TestMe {
    public static void main(String[] args) {

        String payload_signed = String.format("{\"timestamp\":\"%1$d\",\"walletId\":\"acmebank\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}", new Date().getTime());

        System.out.println(payload_signed);
    }

}
